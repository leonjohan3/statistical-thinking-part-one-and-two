# Statistical thinking

## Overview
This repository contains notebooks and other files related to the Datacamp Statistical Thinking in Python Part 1 and 2.

## Exploratory Data Analysis
* Overview: an approach to analyzing data sets in order to summarize their main characteristics.

## Graphical tools
* Box Plot, Histogram, scatter plots, stem and leaf plots

